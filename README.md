# Sublime-Sim
![image](https://gitlab.com/letsracebwoi1/sublime-sim/raw/master/SublimeSimLogo.png)

A full reimplementation of The Sims Online, using Monogame. FreeSO aims to be faithful to the original game as well as include quality of life changes such as hardware rendering, Hi-res output and >2 floor houses. 
Modified for private servers that aren't accessible by the general public.

FreeSO (and therefore Sublime-Sim) currently depends on the original game files (objects, avatars, ui) to function, which are available for download from EA servers.

## Prerequisites
* [Visual Studio 2015](https://www.visualstudio.com/en-us/downloads/visual-studio-2015-downloads-vs.aspx)
* [MonoGame](http://www.monogame.net): 3.5 for the iOS and Android VS2015 project types.

# License
> This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
> If a copy of the MPL was not distributed with this file, You can obtain one at
> http://mozilla.org/MPL/2.0/.